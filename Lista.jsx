import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

//Importamos la funcion para listar TODOS los books
import { apiBooks } from "./api";

const Lista = () => {
  const [books, setBooks] = useState([]);

  //Obtenemos los books y los guardamos en el setBooks
  const obtenerData = async () => {
    const data = await apiBooks();
    setBooks(data);
  };

  //Al cargar la pagina ejecuta la funcion obtenerData
  useEffect(() => {
    obtenerData();
  }, []);

  return (
    <div>
      {books.map((book) => (
        <>
          {/* Al dar click, te redireccionara al formulario con el ID del cupon */}
          <Link to={`/formulario/${book.couponId}`}>{book.couponId}</Link>
          <p>{book.usurio}</p>
          <p>{book.codigo}</p>
          <p>{book.fecha1}</p>
          <p>{book.fecha2}</p>
        </>
      ))}
    </div>
  );
};

export default Lista;

//En tu archivo de rutas poner el :id
// path="/formulario/:id"
