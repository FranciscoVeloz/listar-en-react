import React, { useState, useEffect } from "react";
import { useRouter } from "react-router-dom";

//Importamos la funcion que SOLO te trae un book
import { apiBook } from "./api";

const Formulario = () => {
  //Obtenemos el id de la URL de la pagina web
  const id = useRouter.id;

  //Los valores de tus inputs
  const [txtUsuario, setTxtUsuario] = useState("");
  const [txtFecha1, setTxtFecha1] = useState("");

  //Obtenemos un solo book y lo pasamos a los inputs
  const obtenerData = async () => {
    const data = await apiBook(id);
    setTxtUsuario(data.usurio);
    setTxtFecha1(data.fecha1);
  };

  //Al cargar la pagina, obten el book
  useEffect(() => {
    obtenerData();
  }, []);

  return (
    <div>
      <input type="text" value={txtUsuario} />
      <input type="text" value={txtFecha1} />
    </div>
  );
};

export default Formulario;
