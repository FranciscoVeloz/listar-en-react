//Funcion para obtener TODOS los books
export const apiBooks = async () => {
  const response = await fetch("http://localhost:5001/api/books");
  const books = await response.json();

  const response2 = await fetch("http://localhost:5001/api/coupons");
  const coupons = await response2.json();

  const arregloNuevo = [];

  for (const book of books) {
    for (const coupon of coupons) {
      if (book.couponId === coupon.couponId) {
        const newBook = {
          couponId: coupon.couponId,
          usurio: coupon.usuario,
          codigo: coupon.codigo,
          fecha1: book.fecha1,
          fecha2: book.fecha2,
        };

        arregloNuevo.push(newBook);
      }
    }
  }

  return arregloNuevo;
};

//Funcion para obtener SOLO UN book
export const apiBook = async (couponId) => {
  const response = await fetch(`http://localhost:5001/api/books?couponId=${couponId}`);
  const book = await response.json();

  const response2 = await fetch(`http://localhost:5001/api/coupons?couponId=${couponId}`);
  const coupon = await response2.json();

  const newBook = {
    couponId: coupon.couponId,
    usurio: coupon.usuario,
    codigo: coupon.codigo,
    fecha1: book.fecha1,
    fecha2: book.fecha2,
  };

  return newBook;
};
